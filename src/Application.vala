public class Application : Gtk.Application {
    public Application() {
        Object (
            application_id: "com.github.acvarium.valancia",
            flags: ApplicationFlags.FLAGS_NONE
        );
    }

    protected override void activate () {
        var window = new Valancia.Window (this);
        
        add_window(window);
    }
}