public class Valancia.HeaderBar : Gtk.HeaderBar {
    public Valancia.Window main_window { get; construct; }

    public HeaderBar (Valancia.Window window){
        Object (
            main_window: window
        );
    }
    construct {
        show_close_button = true;
        title = "Valancia";
        subtitle = "Node based image editor";
        var add_button = new Gtk.Button.with_label("Add");
        add_button.get_style_context().add_class("suggested-action");
        add_button.valign = Gtk.Align.CENTER;
        add_button.clicked.connect(() => { main_window.add_node();});
        pack_start(add_button);     

    }
}
