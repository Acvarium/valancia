using GFlow;

public class Valancia.Window : Gtk.ApplicationWindow {

    GtkFlow.NodeView nv;

    public Window(Application app){
        Object (
            application: app
        );
    }
    construct {
        window_position = Gtk.WindowPosition.CENTER;
        maximize();
        var headerbar = new Valancia.HeaderBar(this);
        set_titlebar(headerbar);

        var sw = new Gtk.ScrolledWindow(null,null);
        nv = new GtkFlow.NodeView();
        var box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);

        sw.expand = true;
        box.add(sw);
        sw.add(nv);
        this.add(box);

        show_all();
    }

    public void add_node()
    {
        var o_node = new Valancia.OperationNode();
        var n_node = new Valancia.NumberNode();
        var n2_node = new Valancia.NumberNode();
        var output = new Valancia.PrintNode();


        //  var n = new SimpleNode ();
        //  int sink_v = 0;
        //  int source_v = 0;
        //  GFlow.SimpleSink sink = new GFlow.SimpleSink(sink_v);
        //  GFlow.SimpleSource source = new GFlow.SimpleSource(source_v);
        //  sink.name = "sink";
        //  source.name = "source";
        //  n.add_source(source);
        //  n.add_sink(sink);
        //  n.name = "node";
        nv.add_with_child(o_node, o_node.combobox);
        nv.add_with_child(n_node, n_node.spinbutton);
        nv.add_with_child(n2_node, n2_node.spinbutton);
        nv.add_with_child(output, output.childlabel);



    }
}