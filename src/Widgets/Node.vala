using GFlow;

class Valancia.OperationNode : SimpleNode {
    
    GFlow.SimpleSink summand_a;
    GFlow.SimpleSink summand_b;
    GFlow.SimpleSource result;

    public Gtk.ComboBoxText combobox = new Gtk.ComboBoxText();

    public OperationNode() {
        summand_a = new GFlow.SimpleSink(0);
        summand_b = new GFlow.SimpleSink(0);
        summand_a.highlight = true;
        this.add_sink(summand_a);
        this.add_sink(summand_b);
        summand_a.name = "operand A";
        summand_b.name = "operand B";
        result = new GFlow.SimpleSource(0);
        this.add_source(result);
        result.name = "result";
        //  result.value = Null;
        string[] operations = new string[]{"+", "-", "*"};
        combobox.changed.connect(() => {do_calculation();});
        combobox.entry_text_column = 0;
        foreach (string o in operations) {
            combobox.append_text(o);
        }
        summand_a.changed.connect(() => {do_calculation();});
        summand_b.changed.connect(() => {do_calculation();});
        this.name = "Operator";
    }

    void do_calculation(){
        int _result = 0;
        int value_a = summand_a.get_value().get_int();
        int value_b = summand_b.get_value().get_int();

        switch (combobox.get_active_text()) {
            case "+":
                _result = value_a + value_b;
                break;
            case "-":
                _result = value_a - value_b;
                break;
            case "*":
                _result = value_a * value_b;
                break;
        }
        result.set_value(_result);
    }
}

class Valancia.NumberNode : SimpleNode {
    GFlow.SimpleSource number_source;
    Gtk.Adjustment adjustment;
    GFlow.SimpleSource result;

    public Gtk.SpinButton spinbutton;

    public NumberNode() {
        number_source = new GFlow.SimpleSource(0);
        number_source.name = "output";
        this.add_source(number_source);
        adjustment = new Gtk.Adjustment(1, 0, 100, 1, 10, 0);
        spinbutton = new Gtk.SpinButton(adjustment, 50, 0);
        spinbutton.value_changed.connect(do_value_changed);
        number_source.set_value((int)(spinbutton.value));
        this.name = "NumberGenerator";
    }
    void do_value_changed()
    {
        number_source.set_value((int)(spinbutton.value));
    }
}

class Valancia.PrintNode : SimpleNode {
    public Gtk.Label childlabel;

    SimpleSink number = new GFlow.SimpleSink(0);
    public PrintNode() {
        number.name = "input";
        number.changed.connect(do_print);
        this.add_sink(number);
        childlabel = new Gtk.Label("aaa");
        this.name = "Output";
    }
    void do_print(){
        //  int n = (int)number.get_value();
        childlabel.label = number.get_value().get_int().to_string();
    }   
}